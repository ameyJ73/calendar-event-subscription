package com.amazon.scheduler.handlers;

import com.amazon.scheduler.bo.authorization.AuthProviderData;
import com.amazon.scheduler.bo.subscription.SubscriptionResponse;
import com.amazon.scheduler.bo.user.User;
import com.amazon.scheduler.components.helpers.AuthHelper;
import com.amazon.scheduler.components.helpers.LambdaHelper;
import com.amazon.scheduler.components.helpers.SubscriptionHelper;
import com.amazon.scheduler.components.helpers.UserHelper;
import com.amazon.scheduler.components.managers.UserManager;
import com.amazon.scheduler.dagger.DaggerLambdaInjector;
import com.amazon.scheduler.dagger.LambdaInjector;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

public class AuthRequestReceiver implements RequestHandler<Object, APIGatewayProxyResponseEvent>
{
	private AuthHelper authHelper;
	private UserHelper userHelper;
	private UserManager userManager;
	private SubscriptionHelper subscriptionHelper;
	private LambdaHelper lambdaHelper;

	public AuthRequestReceiver()
	{
		LambdaInjector injector = DaggerLambdaInjector.create();
		authHelper = injector.authHelper();
		userHelper = injector.userHelper();
		userManager = injector.userManager();
		subscriptionHelper = injector.subscriptionHelper();
		lambdaHelper = injector.lambdaHelper();
	}

	public APIGatewayProxyResponseEvent handleRequest(Object event, Context context)
	{
		boolean requestStatus = false;
		LambdaLogger logger = context.getLogger();
		String authenticationCode = lambdaHelper.getAuthCode(event);
		try {
			AuthProviderData authProviderData = authHelper.authenticateRequest(authenticationCode);
			logger.log("SUCCESS : User authenticated");
			User user = userHelper.getUser(authProviderData.getAccess_token());
			SubscriptionResponse response = subscriptionHelper.sendSubscriptionRequest(authProviderData.getAccess_token(),user.getId());
			if(response != null) requestStatus = true;
			logger.log("SUCCESS : Subscription created.");
			userManager.saveUser(user.getId(), user.getUserPrincipalName(), authProviderData.getRefresh_token(), response.getId());
			logger.log("SUCCESS : User saved.");
		} catch (Exception e) {
			logger.log("ERROR : " + e.getMessage());
		}
		return lambdaHelper.getResponse(requestStatus);
	}
}