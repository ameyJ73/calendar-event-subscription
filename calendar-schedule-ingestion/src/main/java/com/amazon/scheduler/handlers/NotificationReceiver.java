package com.amazon.scheduler.handlers;

import static com.amazon.scheduler.constants.Constants.NOTIFICATION_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.PARAM_VALIDATION_TOKEN;
import static com.amazon.scheduler.constants.Constants.HEADER_CONTENTTYPE_TEXT_PLAIN;

import com.amazon.scheduler.dagger.DaggerLambdaInjector;
import com.amazon.scheduler.dagger.LambdaInjector;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazon.scheduler.bo.notification.ChangeNotificationsCollection;
import com.amazon.scheduler.components.helpers.NotificationHelper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class NotificationReceiver 
{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	private NotificationHelper notificationHelper;

    @Inject
    public NotificationReceiver()
    {
        LambdaInjector injector = DaggerLambdaInjector.create();
        notificationHelper = injector.notificationHelper();
    }
	
    @PostMapping(value=NOTIFICATION_ENDPOINT, headers = {HEADER_CONTENTTYPE_TEXT_PLAIN})
    @ResponseBody
    public ResponseEntity<String> handleValidation(@RequestParam(value = PARAM_VALIDATION_TOKEN) String validationToken)
    {
    	LOGGER.info("validation request received");
        return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(validationToken);
    }
    
    @PostMapping(NOTIFICATION_ENDPOINT)
    @ResponseBody
	public ResponseEntity<String> handleNotification(@RequestBody() ChangeNotificationsCollection notifications)
    {
        LOGGER.info("notification received");
        try {
			notificationHelper.processNotification(notifications.getValue().get(0));
			LOGGER.info("notification processed");
		} catch (Exception e) {
			LOGGER.info("ERROR : " + e.getMessage());
		}
        return ResponseEntity.ok().body("");
	}
}