package com.amazon.scheduler.handlers;

import com.amazon.scheduler.bo.lambdaRequest.LambdaRequest;
import com.amazon.scheduler.bo.notification.ChangeNotificationsCollection;
import com.amazon.scheduler.components.helpers.NotificationHelper;
import com.amazon.scheduler.dagger.DaggerLambdaInjector;
import com.amazon.scheduler.dagger.LambdaInjector;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import lombok.SneakyThrows;

import javax.inject.Inject;

public class NotificationReceiverLambda implements RequestHandler<Object, String> {

    private NotificationHelper notificationHelper;

    @Inject
    public NotificationReceiverLambda()
    {
        LambdaInjector injector = DaggerLambdaInjector.create();
        notificationHelper = injector.notificationHelper();
    }

    @Override
    public String handleRequest(Object event, Context context)
    {
        LambdaLogger logger = context.getLogger();
        Gson gson = new Gson();
        String json = gson.toJson(event);
        LambdaRequest lambdaRequest = gson.fromJson(json, LambdaRequest.class);
        try {
            if(lambdaRequest.getQueryStringParameters().getValidationToken() != null)
                return lambdaRequest.getQueryStringParameters().getValidationToken();
            else if(lambdaRequest.getBody() != null)
            {
                notificationHelper.processNotification(lambdaRequest.getBody().getNotifications().getValue().get(0));
                logger.log("SUCCESS : Notification processed.");
                return "";
            }
        } catch (Exception e) {
            logger.log("ERROR : " + e.getMessage());
        }
        return null;
    }
}
