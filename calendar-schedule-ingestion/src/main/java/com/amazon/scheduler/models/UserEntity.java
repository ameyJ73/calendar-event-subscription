package com.amazon.scheduler.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import static com.amazon.scheduler.constants.Constants.DYNAMODB_USER_DATA_TABLE;
import static com.amazon.scheduler.constants.Constants.USER_DATA_TABLE_PRIMARY_KEY;
import static com.amazon.scheduler.constants.Constants.USER_DATA_TABLE_PRINCIPAL_NAME_KEY;
import static com.amazon.scheduler.constants.Constants.USER_DATA_TABLE_REFRESH_TOKEN_KEY;
import static com.amazon.scheduler.constants.Constants.USER_DATA_TABLE_SUBSCRIPTION_ID_KEY;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = DYNAMODB_USER_DATA_TABLE)
public class UserEntity
{
	@DynamoDBHashKey(attributeName = USER_DATA_TABLE_PRIMARY_KEY)
	private String userId;

	@DynamoDBAttribute(attributeName = USER_DATA_TABLE_PRINCIPAL_NAME_KEY)
	private String userPrincipalName;

	@DynamoDBAttribute(attributeName = USER_DATA_TABLE_REFRESH_TOKEN_KEY)
	private String refreshToken;

	@DynamoDBAttribute(attributeName = USER_DATA_TABLE_SUBSCRIPTION_ID_KEY)
	private String subscriptionId;
}
