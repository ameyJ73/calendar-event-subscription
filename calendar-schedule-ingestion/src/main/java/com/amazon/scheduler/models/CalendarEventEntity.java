package com.amazon.scheduler.models;

import static com.amazon.scheduler.constants.Constants.DYNAMODB_EVENT_DATA_TABLE;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_PRIMARY_KEY;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_SUBSCRIPTION_ID_KEY;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_USER_ID_KEY;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_SUBJECT_KEY;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_START_DATE_KEY;
import static com.amazon.scheduler.constants.Constants.EVENT_DATA_TABLE_END_DATE_KEY;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = DYNAMODB_EVENT_DATA_TABLE)
public class CalendarEventEntity
{
	@DynamoDBHashKey(attributeName = EVENT_DATA_TABLE_PRIMARY_KEY)
	private String eventId;

	@DynamoDBAttribute(attributeName = EVENT_DATA_TABLE_USER_ID_KEY)
	private String userId;

	@DynamoDBAttribute(attributeName = EVENT_DATA_TABLE_SUBJECT_KEY)
	private String subject;

	@DynamoDBAttribute(attributeName = EVENT_DATA_TABLE_START_DATE_KEY)
	private String startDate;

	@DynamoDBAttribute(attributeName = EVENT_DATA_TABLE_END_DATE_KEY)
	private String endDate;
}
