package com.amazon.scheduler.dagger;

import com.amazon.scheduler.components.helpers.AuthHelper;
import com.amazon.scheduler.components.helpers.LambdaHelper;
import com.amazon.scheduler.components.helpers.SubscriptionHelper;
import com.amazon.scheduler.components.helpers.UserHelper;
import com.amazon.scheduler.components.helpers.NotificationHelper;
import com.amazon.scheduler.components.managers.UserManager;
import dagger.Component;

@Component
public interface LambdaInjector
{
	AuthHelper authHelper();
	SubscriptionHelper subscriptionHelper();
	UserHelper userHelper();
	LambdaHelper lambdaHelper();
	UserManager userManager();
    NotificationHelper notificationHelper();
}
