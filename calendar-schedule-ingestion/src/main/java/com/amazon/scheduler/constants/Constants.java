package com.amazon.scheduler.constants;

import com.amazonaws.regions.Regions;

public class Constants
{
	public static final String CLIENT_ID = "e61a115b-92f3-4e7d-8109-52ea9d2c10a5";
	public static final String CLIENT_SECRET = "Ut2Km7Hd1w_FNG.JuU5WeBpP.~.e6k76j7";
	public static final String SCOPES = "offline_access openid User.Read Calendars.Read";
	public static final String REDIRECT_URI = "https://nst8ejvec4.execute-api.us-east-1.amazonaws.com/prod/lambda-api-java-example/";
	public static final String NOTIFICATION_URI = "https://007faa00bcbf.ngrok.io/notification";

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.mmm'Z'";
	public static final String PARAM_VALIDATION_TOKEN = "validationToken";
	public static final String HEADER_CONTENTTYPE_TEXT_PLAIN = "content-type=text/plain";
	public static final String APPLICATION_JSON = "application/json";
	public static final String TOKEN_TYPE = "Bearer ";


	public static final String MICROSOFT_GRAPH_API_ENDPOINT = "https://graph.microsoft.com/";
	public static final String MICROSOFT_TOKEN_PROCESSING_ENDPOINT = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
	public static final String CURRENT_USER_ENDPOINT = "v1.0/me";
	public static final String USER_ENDPOINT = "/users";
	public static final String NOTIFICATION_ENDPOINT = "/notification";
	public static final String SUBSCRIPTION_ENDPOINT = "v1.0/subscriptions";
	public static final String EVENTS_ENDPOINT = "/events";
	public static final String RESOURCE_PREFIX = "v1.0/";
	public static final String SECRET_CLIENT_STATE = "SecretClientState";
	public static final String CHANGETYPE_CREATED = "created";
	public static final String CHANGETYPE_UPDATED = "updated";
	public static final String CHANGETYPE_DELETED = "deleted";
	public static final String CHANGE_TYPE_LIST = String.join(",", CHANGETYPE_CREATED,CHANGETYPE_UPDATED,CHANGETYPE_DELETED);

	public static final String ACCESS_KEY = "AKIARYNFXYDYJQZFZEYZ";
	public static final String SECRET_KEY = "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";

	public static final String DYNAMODB_USER_DATA_TABLE = "user-token-data";
	public static final String DYNAMODB_EVENT_DATA_TABLE = "event-data";
	public static final Regions REGION = Regions.US_EAST_1;
	public static final String EVENT_DATA_TABLE_PRIMARY_KEY = "event-id";
	public static final String EVENT_DATA_TABLE_SUBSCRIPTION_ID_KEY = "subscription-id";
	public static final String EVENT_DATA_TABLE_USER_ID_KEY = "user-id";
	public static final String EVENT_DATA_TABLE_SUBJECT_KEY = "subject";
	public static final String EVENT_DATA_TABLE_END_DATE_KEY = "end-date";
	public static final String EVENT_DATA_TABLE_START_DATE_KEY = "start-date";
	public static final String USER_DATA_TABLE_PRIMARY_KEY = "user-id";
	public static final String USER_DATA_TABLE_PRINCIPAL_NAME_KEY = "user-principal-name";
	public static final String USER_DATA_TABLE_REFRESH_TOKEN_KEY = "refresh-token";
	public static final String USER_DATA_TABLE_SUBSCRIPTION_ID_KEY = "subscription-id";

	public static final String GRANT_TYPE_KEY = "grant_type";
	public static final String CLIENT_ID_KEY = "client_id";
	public static final String CLIENT_SECRET_KEY = "client_secret";
	public static final String SCOPE_KEY = "scope";
	public static final String REDIRECT_URI_KEY = "redirect_uri";
	public static final String AUTHORIZATION_CODE_KEY = "code";
	public static final String REFRESH_TOKEN_KEY = "refresh_token";
	public static final String REFRESH_TOKEN = "refresh_token";
	public static final String AUTHORIZATION_CODE = "authorization_code";

	public static final String EVENT_SUBJECT = "Amazon";

	public static final String SUCCESS_RESPONSE = "success.html";
	public static final String ERROR_RESPONSE = "error.html";
}
