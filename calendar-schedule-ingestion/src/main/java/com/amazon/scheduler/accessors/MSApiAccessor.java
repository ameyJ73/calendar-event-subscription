package com.amazon.scheduler.accessors;

import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import com.amazon.scheduler.bo.authorization.AuthProviderData;
import com.amazon.scheduler.bo.subscription.SubscriptionBody;
import com.amazon.scheduler.bo.subscription.SubscriptionResponse;
import com.amazon.scheduler.components.utils.HttpCommunicationManager;
import com.google.gson.Gson;

import static com.amazon.scheduler.constants.Constants.CHANGE_TYPE_LIST;
import static com.amazon.scheduler.constants.Constants.CLIENT_ID;
import static com.amazon.scheduler.constants.Constants.CLIENT_SECRET;
import static com.amazon.scheduler.constants.Constants.EVENTS_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.MICROSOFT_GRAPH_API_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.MICROSOFT_TOKEN_PROCESSING_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.NOTIFICATION_URI;
import static com.amazon.scheduler.constants.Constants.REDIRECT_URI;
import static com.amazon.scheduler.constants.Constants.SCOPES;
import static com.amazon.scheduler.constants.Constants.SECRET_CLIENT_STATE;
import static com.amazon.scheduler.constants.Constants.SUBSCRIPTION_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.USER_ENDPOINT;
import static com.amazon.scheduler.constants.Constants.TOKEN_TYPE;
import static com.amazon.scheduler.constants.Constants.APPLICATION_JSON;
import static com.amazon.scheduler.constants.Constants.DATE_FORMAT;
import static com.amazon.scheduler.constants.Constants.GRANT_TYPE_KEY;
import static com.amazon.scheduler.constants.Constants.CLIENT_ID_KEY;
import static com.amazon.scheduler.constants.Constants.CLIENT_SECRET_KEY;
import static com.amazon.scheduler.constants.Constants.SCOPE_KEY;
import static com.amazon.scheduler.constants.Constants.REFRESH_TOKEN_KEY;
import static com.amazon.scheduler.constants.Constants.REDIRECT_URI_KEY;
import static com.amazon.scheduler.constants.Constants.AUTHORIZATION_CODE_KEY;
import static com.amazon.scheduler.constants.Constants.AUTHORIZATION_CODE;
import static com.amazon.scheduler.constants.Constants.REFRESH_TOKEN;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class MSApiAccessor
{
	private final HttpCommunicationManager httpCommunicationManager;
	
	@Inject
	public MSApiAccessor(HttpCommunicationManager httpCommunicationManager)
	{
		this.httpCommunicationManager = httpCommunicationManager;
	}
	
	public AuthProviderData getAuthProviderData(String authenticationCode) throws Exception
	{
		List<NameValuePair> urlParameters = getUrlParametersForAccessToken(authenticationCode);
		HttpUriRequest request = RequestBuilder.post()
				  .setUri(MICROSOFT_TOKEN_PROCESSING_ENDPOINT)
				  .setEntity(new UrlEncodedFormEntity(urlParameters))
				  .build();
		return httpCommunicationManager.execute(request, AuthProviderData.class);
	}
	
	public AuthProviderData refreshAuthProviderData(String refreshToken) throws Exception
	{
		List<NameValuePair> urlParameters = getUrlParametersForRefreshToken(refreshToken);
		HttpUriRequest request = RequestBuilder.post()
				  .setUri(MICROSOFT_TOKEN_PROCESSING_ENDPOINT)
				  .setEntity(new UrlEncodedFormEntity(urlParameters))
				  .build();
		return httpCommunicationManager.execute(request, AuthProviderData.class);
	}
	
	public <T> T getGraphApiResource(Class<T> clazz, String accessToken, String resource) throws Exception
	{
		HttpUriRequest request = RequestBuilder.get()
				  .setUri(MICROSOFT_GRAPH_API_ENDPOINT + resource)
				  .setHeader(HttpHeaders.AUTHORIZATION, TOKEN_TYPE + accessToken)
				  .setHeader(HttpHeaders.ACCEPT, APPLICATION_JSON)
				  .build();
		return httpCommunicationManager.execute(request, clazz);
	}
	
	public SubscriptionResponse createSubscription(String accessToken, String userId) throws Exception
	{
		String requestBody = createSubscriptionRequestBody(userId);
		HttpUriRequest request = RequestBuilder.post()
				  .setUri(MICROSOFT_GRAPH_API_ENDPOINT + SUBSCRIPTION_ENDPOINT)
				  .setHeader(HttpHeaders.AUTHORIZATION, TOKEN_TYPE + accessToken)
				  .setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
				  .setEntity(new StringEntity(requestBody))
				  .build();
		return httpCommunicationManager.execute(request, SubscriptionResponse.class);
	}

	private List<NameValuePair> getUrlParametersForRefreshToken(String refreshToken)
	{
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair(GRANT_TYPE_KEY, REFRESH_TOKEN));
        urlParameters.add(new BasicNameValuePair(CLIENT_ID_KEY, CLIENT_ID));
        urlParameters.add(new BasicNameValuePair(SCOPE_KEY, SCOPES));
        urlParameters.add(new BasicNameValuePair(CLIENT_SECRET_KEY, CLIENT_SECRET));
        urlParameters.add(new BasicNameValuePair(REFRESH_TOKEN_KEY, refreshToken));
        return urlParameters;
	}
	
	private List<NameValuePair> getUrlParametersForAccessToken(String code)
	{
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair(GRANT_TYPE_KEY, AUTHORIZATION_CODE));
        urlParameters.add(new BasicNameValuePair(CLIENT_ID_KEY, CLIENT_ID));
        urlParameters.add(new BasicNameValuePair(REDIRECT_URI_KEY, REDIRECT_URI));
        urlParameters.add(new BasicNameValuePair(CLIENT_SECRET_KEY, CLIENT_SECRET));
        urlParameters.add(new BasicNameValuePair(AUTHORIZATION_CODE_KEY, code));
        return urlParameters;
	}
	
	private String createSubscriptionRequestBody(String userId)
	{
		Gson gson = new Gson();
		String resource = USER_ENDPOINT + "/" + userId + EVENTS_ENDPOINT;
		SubscriptionBody subscriptionBody = SubscriptionBody.builder()
													.changeType(CHANGE_TYPE_LIST)
													.notificationUrl(NOTIFICATION_URI)
													.resource(resource)
													.expirationDateTime(getExpirationDateTime())
													.clientState(SECRET_CLIENT_STATE)
													.build();
		return gson.toJson(subscriptionBody);
	}
	
	private String getExpirationDateTime()
	{
		DateFormat f = new SimpleDateFormat(DATE_FORMAT);
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 2);
        return f.format(cal.getTime());
	}
	
}
