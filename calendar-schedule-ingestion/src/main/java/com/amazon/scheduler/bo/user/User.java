package com.amazon.scheduler.bo.user;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class User
{
	private String id;
	private String userPrincipalName;
	private String displayName;
	private String mobilePhone;
}
