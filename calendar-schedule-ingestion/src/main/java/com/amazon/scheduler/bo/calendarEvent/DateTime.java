package com.amazon.scheduler.bo.calendarEvent;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class DateTime
{
	private String dateTime;
	private String timeZone;
}
