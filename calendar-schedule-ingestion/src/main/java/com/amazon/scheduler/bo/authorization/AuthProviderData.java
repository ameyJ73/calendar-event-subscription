package com.amazon.scheduler.bo.authorization;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AuthProviderData
{
	private String access_token;
	private String refresh_token;
	private String token_type;
	private String scope;
	private String id_token;
}
