package com.amazon.scheduler.bo.notification;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;

@Getter
@Builder
public class ChangeNotificationsCollection 
{
    private ArrayList<ChangeNotification> value;
    private ArrayList<String> validationTokens;   
}