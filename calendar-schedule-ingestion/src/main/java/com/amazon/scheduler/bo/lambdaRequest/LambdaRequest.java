package com.amazon.scheduler.bo.lambdaRequest;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LambdaRequest
{
	private String httpMethod;
	private String path;
	private QueryStringParameters queryStringParameters;
	private Body body;
}