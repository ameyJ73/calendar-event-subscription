package com.amazon.scheduler.bo.lambdaRequest;

import com.amazon.scheduler.bo.notification.ChangeNotificationsCollection;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Body
{
    private ChangeNotificationsCollection notifications;
}
