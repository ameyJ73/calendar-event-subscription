package com.amazon.scheduler.bo.lambdaRequest;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class QueryStringParameters 
{
	private String code;
	private String state;
	private String validationToken;
}
