package com.amazon.scheduler.bo.subscription;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SubscriptionResponse 
{
	private String id;
	private String resource;
	private String changeType;
	private String notificationUrl;
	private String expirationDateTime;
}
