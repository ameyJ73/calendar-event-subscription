package com.amazon.scheduler.bo.calendarEvent;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CalendarEvent
{
	private String id;
	private String subject;
	private String bodyPreview;
	private DateTime start;
	private DateTime end;
}
