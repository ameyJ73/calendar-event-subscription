package com.amazon.scheduler.bo.notification;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ChangeNotification 
{
    private String subscriptionId;
    private String changeType;
    private String resource;
    private String clientState;    
}