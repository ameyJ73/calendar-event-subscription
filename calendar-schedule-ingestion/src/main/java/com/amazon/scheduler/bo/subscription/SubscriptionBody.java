package com.amazon.scheduler.bo.subscription;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SubscriptionBody
{
	private String changeType;
	private String notificationUrl;
	private String resource;
	private String expirationDateTime;
	private String clientState;
}