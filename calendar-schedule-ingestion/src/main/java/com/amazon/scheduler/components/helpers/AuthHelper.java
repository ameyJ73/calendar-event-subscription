package com.amazon.scheduler.components.helpers;

import javax.inject.Inject;

import com.amazon.scheduler.accessors.MSApiAccessor;
import com.amazon.scheduler.bo.authorization.AuthProviderData;

public class AuthHelper
{
	private final MSApiAccessor msApiAccessor;
	
	@Inject
	public AuthHelper(MSApiAccessor msApiAccessor)
	{
		this.msApiAccessor = msApiAccessor;
	}
	
	public AuthProviderData authenticateRequest(String authenticationCode) throws Exception
	{
		AuthProviderData authProviderData = msApiAccessor.getAuthProviderData(authenticationCode);
		return authProviderData;
	}
	
	public AuthProviderData refreshAuthProviderData(String refreshToken) throws Exception
	{
		AuthProviderData authProviderData = msApiAccessor.refreshAuthProviderData(refreshToken);
		return authProviderData;
	}
   
}