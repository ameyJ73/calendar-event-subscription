package com.amazon.scheduler.components.helpers;

import javax.inject.Inject;

import com.amazon.scheduler.accessors.MSApiAccessor;
import com.amazon.scheduler.bo.calendarEvent.CalendarEvent;

public class CalendarEventHelper
{
	private final MSApiAccessor msApiAccessor;
	
	@Inject
	public CalendarEventHelper(MSApiAccessor msApiAccessor)
	{
		this.msApiAccessor = msApiAccessor;
	}
	
	public CalendarEvent getCalendarEvent(String accessToken, String resource) throws Exception
	{
		CalendarEvent calendarEvent = msApiAccessor.getGraphApiResource(CalendarEvent.class, accessToken, resource);		
		return calendarEvent;
	}
	
}
