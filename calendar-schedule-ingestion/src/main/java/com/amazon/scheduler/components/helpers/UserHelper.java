package com.amazon.scheduler.components.helpers;

import static com.amazon.scheduler.constants.Constants.CURRENT_USER_ENDPOINT;

import javax.inject.Inject;

import com.amazon.scheduler.accessors.MSApiAccessor;
import com.amazon.scheduler.bo.authorization.AuthProviderData;
import com.amazon.scheduler.bo.user.User;
import com.amazon.scheduler.components.managers.UserManager;

public class UserHelper 
{
	private final MSApiAccessor msApiAccessor;
	
	@Inject
	public UserHelper(MSApiAccessor msApiAccessor)
	{
		this.msApiAccessor = msApiAccessor;
	}
	
	public User getUser(String accessToken) throws Exception
	{
		User user = msApiAccessor.getGraphApiResource(User.class, accessToken, CURRENT_USER_ENDPOINT);		
		return user;
	}

}
