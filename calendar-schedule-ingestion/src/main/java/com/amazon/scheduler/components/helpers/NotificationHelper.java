package com.amazon.scheduler.components.helpers;

import static com.amazon.scheduler.constants.Constants.EVENT_SUBJECT;
import static com.amazon.scheduler.constants.Constants.RESOURCE_PREFIX;

import javax.inject.Inject;

import com.amazon.scheduler.bo.calendarEvent.CalendarEvent;
import com.amazon.scheduler.bo.notification.ChangeNotification;
import com.amazon.scheduler.components.managers.CalendarEventManager;
import com.amazon.scheduler.components.managers.UserManager;

public class NotificationHelper 
{
	private final CalendarEventHelper calendarEventHelper;
	private final CalendarEventManager calendarEventManager;
	private final UserManager userManager;
	
	@Inject
	public NotificationHelper(CalendarEventHelper calendarEventHelper,
			CalendarEventManager calendarEventManager, UserManager userManager) 
	{
		this.calendarEventHelper = calendarEventHelper;
		this.calendarEventManager = calendarEventManager;
		this.userManager = userManager;
	}

	public void processNotification(ChangeNotification changeNotification) throws Exception
	{
		String changeType = changeNotification.getChangeType();
		String resource = changeNotification.getResource();
		String userId = parseUserId(resource);
		String accessToken = userManager.getAccessToken(userId);
		CalendarEvent calendarEvent = calendarEventHelper.getCalendarEvent(accessToken, RESOURCE_PREFIX+resource);
		if(!calendarEvent.getSubject().equalsIgnoreCase(EVENT_SUBJECT)) return;
		else calendarEventManager.saveEvent(userId, changeType, calendarEvent);
	}
	
	private String parseUserId(String resourceString)
	{
		String[] resourceStringParts = resourceString.split("/");
		return resourceStringParts[1];
	}
	
}
