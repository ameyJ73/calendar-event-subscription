package com.amazon.scheduler.components.helpers;

import static com.amazon.scheduler.constants.Constants.ERROR_RESPONSE;
import static com.amazon.scheduler.constants.Constants.SUCCESS_RESPONSE;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

import javax.inject.Inject;

import com.amazon.scheduler.bo.lambdaRequest.LambdaRequest;
import com.amazon.scheduler.handlers.AuthRequestReceiver;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import org.apache.http.HttpStatus;

public class LambdaHelper 
{
	@Inject
	public LambdaHelper() {}
	
	public String getAuthCode(Object event)
	{
		Gson gson = new Gson();
		String json = gson.toJson(event);
		LambdaRequest lambdaRequest = gson.fromJson(json, LambdaRequest.class);
		return lambdaRequest.getQueryStringParameters().getCode();
	}
	
	public APIGatewayProxyResponseEvent getResponse(boolean status)
	{
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
	    response.setIsBase64Encoded(false);
	    response.setStatusCode(HttpStatus.SC_OK);
	    HashMap<String, String> headers = new HashMap<String, String>();
	    headers.put("Content-type", "text/html");
	    response.setHeaders(headers);
	    response.setBody(getResponseHTML(status));
	    return response;
	}
	
	private String getResponseHTML(boolean status)
	{
		String fileName = status ? SUCCESS_RESPONSE : ERROR_RESPONSE;
		ClassLoader classLoader = new AuthRequestReceiver().getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		String content = null;
		try {
			content = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
}
