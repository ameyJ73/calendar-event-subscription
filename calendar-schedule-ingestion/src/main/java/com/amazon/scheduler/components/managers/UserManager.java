package com.amazon.scheduler.components.managers;

import com.amazon.scheduler.bo.authorization.AuthProviderData;
import com.amazon.scheduler.components.helpers.AuthHelper;
import com.amazon.scheduler.components.utils.DynamoDBHelper;
import com.amazon.scheduler.models.UserEntity;

import javax.inject.Inject;

public class UserManager
{
	private final DynamoDBHelper dynamoDBHelper;
	private final AuthHelper authHelper;

	@Inject
	public UserManager(DynamoDBHelper dynamoDBHelper, AuthHelper authHelper)
	{
		this.dynamoDBHelper = dynamoDBHelper;
		this.authHelper = authHelper;
	}

	public String getAccessToken(String userId) throws Exception
	{
		UserEntity userEntity = getUserEntity(userId);
		String refreshToken = userEntity.getRefreshToken();
		AuthProviderData authProviderData = authHelper.refreshAuthProviderData(refreshToken);
		userEntity.setRefreshToken(authProviderData.getRefresh_token());
		dynamoDBHelper.putItem(userEntity);
		return authProviderData.getAccess_token();
	}

	public void saveUser(String id, String userPrincipalName, String refresh_token, String subscription_id)
	{
		UserEntity userEntity = createUserEntity(id, userPrincipalName, refresh_token, subscription_id);
		dynamoDBHelper.putItem(userEntity);
	}

	private UserEntity createUserEntity(String userId, String userPrincipalName, String refreshToken, String subscriptionId)
	{
		UserEntity userEntity = UserEntity.builder()
				.userId(userId)
				.userPrincipalName(userPrincipalName)
				.refreshToken(refreshToken)
				.subscriptionId(subscriptionId)
				.build();
		return userEntity;
	}

	private UserEntity getUserEntity(String userId)
	{
		UserEntity userEntity = dynamoDBHelper.getItem(UserEntity.class, userId);
		return userEntity;
	}

}
