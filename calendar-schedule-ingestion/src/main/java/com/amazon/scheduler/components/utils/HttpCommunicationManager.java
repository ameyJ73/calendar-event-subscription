package com.amazon.scheduler.components.utils;

import javax.inject.Inject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import lombok.NonNull;

public class HttpCommunicationManager
{
	@Inject
	public HttpCommunicationManager() {}
	
	public <T> T execute(@NonNull final HttpUriRequest request, @NonNull final Class<T> clazz) 
    {
    	Gson gson = new Gson();
        final HttpResponse response = execute(request);
        final String json;
        try {
            json = EntityUtils.toString(response.getEntity());
            return gson.fromJson(json, clazz);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Error in parsing response [%s] for input [%s]", response,
                    request), e);
        }
    }
	
	public HttpResponse execute(@NonNull final HttpUriRequest request) 
    {
        try {
        	CloseableHttpClient httpClient = HttpClients.createDefault();
            final HttpResponse httpResponse = httpClient.execute(request);
            return httpResponse;
        } catch (Exception e) {
            throw new RuntimeException("Error in fetching response for " + request.getURI(), e);
        }
    }

}
