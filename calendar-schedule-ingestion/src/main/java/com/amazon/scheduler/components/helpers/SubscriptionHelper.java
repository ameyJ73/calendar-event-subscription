package com.amazon.scheduler.components.helpers;

import javax.inject.Inject;

import com.amazon.scheduler.accessors.MSApiAccessor;
import com.amazon.scheduler.bo.authorization.AuthProviderData;
import com.amazon.scheduler.bo.subscription.SubscriptionResponse;
import com.amazon.scheduler.bo.user.User;

public class SubscriptionHelper 
{
	private final MSApiAccessor msApiAccessor;
	
	@Inject
	public SubscriptionHelper(MSApiAccessor msApiAccessor)
	{
		this.msApiAccessor = msApiAccessor;
	}

	public SubscriptionResponse sendSubscriptionRequest(String accessToken, String userId) throws Exception
	{
		SubscriptionResponse subscriptionResponse = msApiAccessor.createSubscription(accessToken, userId);
		return subscriptionResponse;
	}
	
}
