package com.amazon.scheduler.components.managers;

import static com.amazon.scheduler.constants.Constants.CHANGETYPE_DELETED;

import javax.inject.Inject;

import com.amazon.scheduler.bo.calendarEvent.CalendarEvent;
import com.amazon.scheduler.components.utils.DynamoDBHelper;
import com.amazon.scheduler.models.CalendarEventEntity;

public class CalendarEventManager 
{
	private final DynamoDBHelper dynamoDBHelper;
	
	@Inject
	public CalendarEventManager(DynamoDBHelper dynamoDBHelper)
	{
		this.dynamoDBHelper = dynamoDBHelper;
	}
	
	public void saveEvent(String userId, String changeType, CalendarEvent calendarEvent)
	{
		CalendarEventEntity calendarEventEntity = createEventEntity(userId, calendarEvent);
		if(changeType.equals(CHANGETYPE_DELETED)) dynamoDBHelper.deleteItem(calendarEventEntity);
		else dynamoDBHelper.putItem(calendarEventEntity);
	}
	
	private CalendarEventEntity createEventEntity(String userId,CalendarEvent calendarEvent)
	{
		CalendarEventEntity calendarEventEntity = CalendarEventEntity.builder()
								.eventId(calendarEvent.getId())
								.userId(userId)
								.subject(calendarEvent.getSubject())
								.startDate(calendarEvent.getStart().getDateTime())
								.endDate(calendarEvent.getEnd().getDateTime())
								.build();
		return calendarEventEntity;
	}
	
}
