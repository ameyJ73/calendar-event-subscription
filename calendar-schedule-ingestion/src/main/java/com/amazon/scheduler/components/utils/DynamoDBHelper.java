package com.amazon.scheduler.components.utils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import javax.inject.Inject;

import static com.amazon.scheduler.constants.Constants.REGION;
import static com.amazon.scheduler.constants.Constants.ACCESS_KEY;
import static com.amazon.scheduler.constants.Constants.SECRET_KEY;

public class DynamoDBHelper
{
    private final DynamoDBMapper mapper;

    @Inject
    public DynamoDBHelper()
    {
        final AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
        final AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(REGION)
                .withCredentials(credentialsProvider)
                .build();
        this.mapper = new DynamoDBMapper(client);
    }

    public void putItem(Object item)
    {
        mapper.save(item);
    }

    public <T> T getItem(Class<T> clazz, String primaryKeyValue)
    {
        return mapper.load(clazz, primaryKeyValue);
    }

    public void deleteItem(Object item)
    {
        mapper.delete(item);
    }

}
